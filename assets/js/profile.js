let token = localStorage.getItem("token");
let username = document.querySelector("#username");
let mobileNo = document.querySelector("#mobileNo");
let email = document.querySelector("#email");
let adminUser = localStorage.getItem("isAdmin");

if(adminUser =="false" || !adminUser){
fetch("https://peaceful-eyrie-10978.herokuapp.com/api/users/details", {
    method: "GET",
    headers: {
        Authorization: `Bearer ${token}`,
    },
})
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        username.innerHTML = data.firstname + " " + data.lastname;
        mobileNo.innerHTML = data.mobileNo;
        email.innerHTML = data.email;

        let courseData
        let courseData2 = data.enrollments
        courseData2.map((course) => {
            fetch(`https://peaceful-eyrie-10978.herokuapp.com/api/courses/${course.courseId}`)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data)
                    courseData = data
                    courseData =
                        (`
                        <div class="col-md-6 my-3">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">${courseData.name}</h5>
                                    <p class="card-text text-left">${courseData.description}</p>
                                    <p class="card-text text-right">${courseData.price}</p>
                                </div>
                            </div>
                        </div>
                        `)
                    container.innerHTML += courseData
                })
            let container = document.querySelector("#coursesContainer");
        })
    })
} else {
    
}