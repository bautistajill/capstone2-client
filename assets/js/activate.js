let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")

	fetch(`https://peaceful-eyrie-10978.herokuapp.com/api/courses/${courseId}/activate`, {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},
		body: JSON.stringify ({
			courseId : courseId
		})
	})
	.then(res => {return res.json()})
	.then(data => {
		if (data){
			alert("Course successfully activated")
			window.location.replace("./courses.html")
		} else {
			alert("Activate failed")
		}
	})
	


