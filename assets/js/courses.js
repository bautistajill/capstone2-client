let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter; 
let cardActive


let navbar =document.querySelector("#profileNav")
let profileNav =document.querySelector("#profileNav")

if (adminUser == "false" || !adminUser) {
	navbar.innerHTML = 
	`
		<a href="./profile.html" class="nav-link"> Profile </a>
	`
	
} else {

	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" style="display:none;"> Profile </a>
	</li>
	`
}


if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary">
			Add Course
		</a>
	</div>

	`
}

if(adminUser =="false" || !adminUser){

fetch('https://peaceful-eyrie-10978.herokuapp.com/api/courses/active')
.then(res => res.json())
.then(data => {
	let courseData;

	if(data.length < 1) {
		courseData = "No courses available"
	} else {
		courseData = data.map(course => {

			cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id}
				class = "btn btn-primary text-white btn-block">Go to Course</a>`	
		
		return (
			`
			<div class="col-md-6 my-3">
				<div class="card">
					<div class ="card-body">
						<h5 class ="card-title">${course.name}</h5>
						<p class="card-text text-right">
						${course.description}
						</p>
						<p class="card-text text-right">
						${course.price}
						</p>
					</div>
					<div class="card-footer">
						${cardFooter}
					</div>
				</div>
			</div>		

			`
			)
		}).join("")
	}

	let container = document.querySelector("#coursesContainer");

	container.innerHTML = courseData;
	})

} else {

	fetch('https://peaceful-eyrie-10978.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		let courseData;

		if(data.length < 1) {
			courseData = "No courses available";
		} else {
			courseData = data.map(course => {
				if(`${course.isActive}` == "false"){
					cardFooter =`<a href="./activate.html?courseId=${course._id}" value=${course._id} 
						class = "btn btn-success text-white btn-block">Activate Course </a>`
					cardActive = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block">Course Details</a>`
				} else {
					cardFooter =`<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} 
						class = "btn btn-danger text-white btn-block">Archive Course </a>`;
					cardActive = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block">Course Details</a>`
					
				}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class ="card-body">
							<h5 class ="card-title">${course.name}</h5>
							<p class="card-text text-right">
							${course.description}
							</p>
							<p class="card-text text-right">
							${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
							${cardActive}
						</div>
					</div>
				</div>		

				`
				)
			}).join("")
		}

		let container = document.querySelector("#coursesContainer");

		container.innerHTML = courseData;
	})
}