let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")


let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")
let student = document.getElementById("stud")

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

let navbar =document.querySelector("#profileNav")
let profileNav =document.querySelector("#profileNav")

if (adminUser == "false" || !adminUser) {
	navbar.innerHTML = 
	`
		<a href="./profile.html" class="nav-link" id="profileNav"> Profile </a>
	`
} else {

	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" style="display:none;"> Profile </a>
	</li>
	`
}

fetch(`https://peaceful-eyrie-10978.herokuapp.com/api/courses/${courseId}`)
.then(res => { return res.json() })
.then(data => {
	

	courseName.innerHTML = data.name 
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	 if (adminUser === "true") {
   
      	stud.innerHTML = `<h1 id="" class="text-center">Enrollees</h1>`

      let userData
      let userData2 = data.enrollees
      userData2.map((user) => {
        fetch(`https://peaceful-eyrie-10978.herokuapp.com/api/users/${user.userId}`, {
        	method: "GET",
        	headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${token}`
			} 
        })

          .then((res) => res.json())
          .then((data) => {
          
            userData = data
            userData = (`
            <div class="col-md-6 my-3">     
            	<div class="card">
               		 <div class="card-body">
                 		 <h5 class="card-title">${userData.firstname} ${userData.lastname} </h5>                                    
               		 </div>
              	</div>
            </div>
            `)
            container.innerHTML += userData
          })
        let container = document.querySelector("#userContainer");
      })
    } else {

	enrollContainer.innerHTML = `<button id = "enrollButton" class ="btn btn-block btn-primary">Enroll</button>`

	document.querySelector("#enrollButton").addEventListener("click", () => {

		fetch("https://peaceful-eyrie-10978.herokuapp.com/api/users/enroll", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${token}`
			},
			body: JSON.stringify ({
				courseId : courseId
			})
		})
	})
		.then(res => {return res.json()})
		.then(data => {
		
			if (data){
				alert("You have enrolled successfully")
				window.location.replace("./courses.html")
			} else {
				alert("Enrollment failed")
			}
		})
	}
})